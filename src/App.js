import './App.css';
import Community from './components/Community';
import JoinProgram from './components/JoinProgram';

function App() {
  return (
    <div className="App">
      <div id='app-container'>
        <Community />
        <JoinProgram />
      </div>
    </div>
  );
}

export default App;
