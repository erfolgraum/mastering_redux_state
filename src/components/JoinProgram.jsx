import React, { useState } from "react";

const JoinProgram = () => {
  const [email, setEmail] = useState("");
  const [subscribed, setSubscribed] = useState(false);
  const [loading, setLoading] = useState(false);

  // const handleOnSubmit = (e) => {
  //   e.preventDefault();
  //   // setSubscribed(true);
    // if(subscribed) {
    //   setSubscribed(true);
    // } 
    // setSubscribed(false)
  //   setEmail("");
  // };

  const handleSubscribe = (e) => {
    e.preventDefault();
    setLoading(true);
    fetch("/subscribe", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email }),
    })
      .then((response) => {
        setLoading(false);
        setSubscribed(true);
        if (response.status === 200) {
          response.json()
          .then((data) => {
            console.log(data);
          });
          
        } else if (response.status === 422) {
          response.json()
          .then((data) => {
            window.alert(data.error);
          });
        } else {
          console.error(`Unexpected response status: ${response.status}`);
        }
      })
      .catch((error) => {
        setLoading(false);
        console.error(error);
      });

      setSubscribed(false)
  };

  const handleUnsubscribe = (e) => {
    e.preventDefault();
    setLoading(true);
    // Send a POST request to the /unsubscribe endpoint
    const url = "/unsubscribe";
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      // body: JSON.stringify(data),
    };
    fetch(url, options)
      .then((response) => {
        response.json()
        .then((data) => {
          console.log(data);
          setSubscribed(false);
          setEmail('');
        });
      })
      .catch((error) => {
        console.error(error);
      })
      .finally(() => {
        setLoading(false);
      });

      setSubscribed(false)
  };

  return (
    <section className="app-section--join-program">
      <h2 className="app-title app-title--join-section">Join Our Program</h2>
      <h3 className="app-subtitle app-subtitle--join-section">
        Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      </h3>
      <form className="form-join" >
        <input
          className="email"
          placeholder="Email"
          type="email"
          name="emailaddress"
          required="required"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        />
        {!subscribed ? (
          <button
          className="app-section__button app-section__button--subscribe"
          disabled={subscribed}
          style={{ opacity: subscribed ? 0.5 : 1 }}
          onClick={handleSubscribe}
        >
          {loading ? "Subscribing..." : "Subscribe"}
        </button>
        ) : (
          <button
            className="app-section__button app-section__button--subscribe"
            disabled={!subscribed}
            style={{ opacity: !subscribed ? 0.5 : 1 }}
            onClick={handleUnsubscribe}
          >
            {loading ? "Unsubscribing..." : "Unsubscribe"}
          </button>
        )}
      </form>
    </section>
  );
};

export default JoinProgram;